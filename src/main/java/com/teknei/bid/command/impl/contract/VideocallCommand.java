package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class VideocallCommand implements Command {

    @Autowired
    @Qualifier(value = "parseVideocallCommand")
    private Command parseVideocallCommand;
    @Autowired
    @Qualifier(value = "persistVideocallCommand")
    private Command persistVideocallCommand;
    @Autowired
    @Qualifier(value = "storeTasVideocallCommand")
    private Command storeTasVideocallCommand;
    @Autowired
    @Qualifier(value = "statusCommand")
    private Command statusCommand;

    @Override
    public CommandResponse execute(CommandRequest request) {
        CommandResponse parseResponse = parseVideocallCommand.execute(request);
        if(parseResponse.getStatus().equals(Status.VIDEOCALL_QUOBIS_ERROR)){
           parseResponse.setDesc(String.valueOf(parseResponse.getStatus().getValue()));
           parseResponse.setStatus(Status.VIDEOCALL_ERROR);
           saveStatus(request.getId(), Status.VIDEOCALL_QUOBIS_ERROR);
           return parseResponse;
        }
        saveStatus(request.getId(), Status.VIDEOCALL_QUOBIS_OK);
        CommandRequest tasRequest = new CommandRequest();
        tasRequest.setId(request.getId());
        tasRequest.setCallee(parseResponse.getCallee());
        tasRequest.setCaller(parseResponse.getCaller());
        tasRequest.setSelfie(parseResponse.getSelfie());
        CommandResponse tasResponse = storeTasVideocallCommand.execute(tasRequest);
        if(tasResponse.getStatus().equals(Status.VIDEOCALL_TAS_ERROR)){
            saveStatus(request.getId(), Status.VIDEOCALL_TAS_ERROR);
            tasResponse.setDesc(String.valueOf(tasResponse.getStatus().getValue()));
            tasResponse.setStatus(Status.VIDEOCALL_ERROR);
            return tasResponse;
        }
        saveStatus(request.getId(), Status.VIDEOCALL_TAS_OK);
        CommandRequest dbRequest = new CommandRequest();
        dbRequest.setId(request.getId());
        dbRequest.setUserOpeCrea(request.getUserOpeCrea()); //MCG
        CommandResponse dbResponse = persistVideocallCommand.execute(dbRequest);
        if(dbResponse.getStatus().equals(Status.VIDEOCALL_DB_ERROR)){
            saveStatus(request.getId(), Status.VIDEOCALL_DB_ERROR);
            dbResponse.setDesc(String.valueOf(dbResponse.getStatus().getValue()));
            dbResponse.setStatus(Status.VIDEOCALL_ERROR);
            return dbResponse;
        }
        saveStatus(request.getId(), Status.VIDEOCALL_DB_OK);
        dbResponse.setStatus(Status.VIDEOCALL_OK);
        return dbResponse;
    }

    /**
     * Persists the current status for the main process
     * @param id
     * @param status
     * @return
     */
    private CommandResponse saveStatus(Long id, Status status){
        CommandRequest request = new CommandRequest();
        request.setId(id);
        request.setRequestStatus(status);
        request.setUserOpeCrea("tkn-api"); //MCG
        CommandResponse response = statusCommand.execute(request);
        return response;
    }
}
