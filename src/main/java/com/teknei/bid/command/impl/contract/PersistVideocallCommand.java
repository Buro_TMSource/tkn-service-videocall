package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.service.VideoCallDBService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersistVideocallCommand implements Command {

    @Autowired
    private VideoCallDBService videoCallDBService;
    private static final Logger log = LoggerFactory.getLogger(PersistVideocallCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setId(request.getId());
        try {
            videoCallDBService.addRecordToDB(request.getId(), "tsrv-vcall");
            commandResponse.setStatus(Status.VIDEOCALL_DB_OK);
        } catch (Exception e) {
            commandResponse.setStatus(Status.VIDEOCALL_DB_ERROR);
        }
        return commandResponse;
    }


}
