package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.service.VideoCallTasService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StoreTasVideocallCommand implements Command {

    @Autowired
    private VideoCallTasService videoCallTasService;

    private static final Logger log = LoggerFactory.getLogger(StoreTasVideocallCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setId(request.getId());
        try {
            videoCallTasService.addQuobisRecordingToTAS(request.getCaller(), request.getId());
            videoCallTasService.addQuobisSelfieToTAS(request.getSelfie(), request.getId());
            commandResponse.setStatus(Status.VIDEOCALL_TAS_OK);
        } catch (Exception e) {
            log.error("Error in storeTasVideocallCommand for: {} with message: {}", request, e.getMessage());
            commandResponse.setStatus(Status.VIDEOCALL_TAS_ERROR);
        }
        return commandResponse;
    }


}
