package com.teknei.bid.service.remote;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "QUOBIS-STORAGE-CLIENT", url = "${tkn.quobis.storage.url}")
public interface QuobisStorageServiceRemote {

    @RequestMapping(value = "/{id}/selfie.png", method = RequestMethod.GET)
    byte[] getSelfie(@PathVariable("id") Long id);

}